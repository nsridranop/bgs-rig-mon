# BGS Rig Monitor #

### What is this repository for? ###

* Bootstrap BGS RIG monitoring setup using docker-compose

### How do I get set up? ###

* Clone the repo
* `docker-compose up -d`


### Prometheus reload config ###

* modify scrape_configs in `config.d/prometheus/prometheus.yml` 
* reload config by 
  `curl -X POST http://localhost:9090/-/reload` or
  `docker exec prometheus killall -HUP prometheus`
